# Amazon Polly Connector
Amazon Polly is a web service that makes it easy to synthesize speech from text.

Documentation: https://docs.aws.amazon.com/polly/

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/polly/2016-06-10/openapi.yaml

## Prerequisites

+ Generate an AWS Access Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

